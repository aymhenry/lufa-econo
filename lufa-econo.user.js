// ==UserScript==
// @name        lufa écono
// @description Commande automatique du sac écono
// @version     1
// @include     https://montreal.lufa.com/fr/superMarket/fruits
// @icon        http://lufa.com/images/logos/logo_en.png
// ==/UserScript==


var refresh = 60;//second
var addToBasket = "cono";
var regex = new RegExp(addToBasket,"i");
var numToAdd = 1;
$(document).ready(function() {
    products = document.getElementsByClassName("single-product-wrapper product-wrapper-favorites");
    for(var i=0;i< products.length;i++){
        var prod = products[i];
        var productName = products[i].getElementsByClassName("product-name")[0].textContent.match(/\w.*\w\)?/)[0];
        var matchProduct = productName.match(regex)!=null;
        if(matchProduct){
            var buySection = products[i].getElementsByClassName("buy-buttons")[0];
            var marketOpen = buySection!=null;
            if(marketOpen){
		var nbItems = parseInt(products[i].getElementsByClassName("number-of-items")[0].firstChild.getAttribute("value"));
		if(nbItems<numToAdd){products[i].getElementsByClassName("plus")[0].click();}
		if(nbItems>numToAdd){products[i].getElementsByClassName("minus")[0].click();}
            }
        }
    }
    setTimeout(function(){
	window.location.reload(1);
    }, refresh*1000);
});
